class Equipment{
	constructor(equipmentType){
		this.equipmentType = equipmentType;
	}
	printInfo(){
		return `Info: ${this.equipmentType}`
	}
}

class Bulldozer extends Equipment{
	constructor(equipmentType, model, bladeType){
		super(equipmentType);
		this.model = model;
		this.bladeType = bladeType;
	}
	printInfo(){
		return super.printInfo()+`
The bulldozer ${this.model} has a ${this.bladeType} blade`
	}
}

let bulldozer1 = new Bulldozer("Bulldozer", "Brute", "Shovel");
console.log(bulldozer1.printInfo())

class TowerCrane extends Equipment{
	constructor(equipmentType, model, hookRadius, maxCapacity){
		super(equipmentType);
		this.model = model;
		this.hookRadius = hookRadius;
		this.maxCapacity = maxCapacity;
	}
	printInfo(){
		return super.printInfo()+`
The tower crane ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`
	}
}

let towerCrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500)
console.log(towerCrane1.printInfo())

class BackLoader extends Equipment{
	constructor(equipmentType, model, type, tippingLoad){
		super(equipmentType);
		this.model = model;
		this.type = type;
		this.tippingLoad = tippingLoad;
	}
	printInfo(){
		return super.printInfo()+`
The loader ${this.model} is a ${this.type} loader and has a tipping load of ${this.tippingLoad} lbs.`
	}
}
let backLoader1 = new BackLoader("back loader", "Turtle", "hydraulic", 1500);
console.log(backLoader1.printInfo())

////////////////////////////////////////////////////////////

class RegularShape{
	constructor(noSides, length){
		this.noSides = noSides;
		this.length = length;
	}
	perimeter(){
		return `The perimeter is `;
	}
	area(){
		return `The area is `
	}

}

class Triangle extends RegularShape{
	perimeter(){
		let perimeter = this.noSides * this.length;
		return super.perimeter() + perimeter
	}
	area(){
		let area = (Math.sqrt(3)/4*(this.length**2));
		return super.area() + area;
	}
}

let triangle1 = new Triangle(3, 10);
console.log(triangle1.perimeter())
console.log(triangle1.area())

class Square extends RegularShape{
	perimeter(){
		let perimeter = this.noSides * this.length;
		return super.perimeter() + perimeter
	}
	area(){
		let area = this.length**2;
		return super.area() + area;
	}
}

let square1 = new Square(4,12);
console.log(square1.perimeter())
console.log(square1.area())

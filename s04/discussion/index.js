class Person{
	constructor(){
		if(this.constructor === Person){
			throw new Error(
				"Object cannot be created from an abstract class Person"
			);
		}
		if(this.getLastName === undefined){
			throw new Error(
				"Class must implement getLastName() method."
			);
		}

	}
}

class Employee extends Person{
	/*Encapsulation aided by private fields (#), ensures data protection. Setters and getters provide controlled access to encapsulated data.*/
	/*Private fields*/
	#firstName;
	#lastName;
	#employeeID;
	constructor(firstName, lastName, employeeID){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#employeeID = employeeID;
	}
	// getter methods
	getFirstName(){
		return `First Name: ${this.#firstName}`
	}
	getLastName(){
		return `Last Name: ${this.#lastName}`
	}
	getFullName(){
		return `${this.#firstName} ${this.#lastName} has employee ID of ${this.#employeeID}.`
	}
	setFirstName(firstName){
		this.#firstName = firstName
	}
	setLastName(lastName){
		this.#lastName = lastName
	}
	setEmployeeID(employeeID){
		this.#employeeID = employeeID
	}
}

// 
const employeeA = new Employee("John", "Smith", "EM-001");
// Direct access with the field/property firstName will return undefined because the property is private
// console.log(employeeA.firstName);

// We could directly change the value of the property firstName because  the property is private.
employeeA.firstName = "David"

console.log(employeeA.getFirstName());
employeeA.setFirstName("David");
console.log(employeeA.getFirstName());
console.log(employeeA.getFullName());

const employeeB = new Employee();
console.log(employeeB.getFullName());

/*employeeB.firstName = "Jill";
employeeB.lastName = "Hill";
employeeB.employeeID = "EM-002";
console.log(employeeB.getFullName());*/

employeeB.setFirstName("Jill");
employeeB.setLastName("Hill");
employeeB.setEmployeeID("EM-002");
console.log(employeeB.getFullName());
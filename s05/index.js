class Request{
	constructor(requesterEmail, content){
		this.requesterEmail = requesterEmail;
		this.content = content;
		this.dateRequested = new Date();
	}
}
let request1 = new Request("ayin@mail.com", "hello");
let request2 = new Request("pao@mail.com", "hi");

class Person{
	constructor(){
		if(this.constructor === Person){
			throw new Error(
				"Object cannot be created from an abstract class Person"
			);
		}
		if(this.getFullName === undefined){
			throw new Error(
				"Class must implement getFullName() method."
			);
		}
		if(this.login === undefined){
			throw new Error(
				"Class must implement login() method."
			);
		}
		if(this.logout === undefined){
			throw new Error(
				"Class must implement logout() method."
			);
		}
	}
}

class Employee extends Person{
	#firstName;
	#lastName;
	#email;
	#department;
	#isActive;
	#requests;
	constructor(firstName, lastName, email, department){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#isActive = true;
		this.#requests = [];
/*		if(this.constructor === Employee){
		}*/
		if(this.addRequest === undefined){
			throw new Error(
				"Class must implement addRequest() method."
			);
		}
	}
	getFullName(){
		return `${this.#firstName} ${this.#lastName}`
	}
	login(){
		return `${this.#firstName} ${this.#lastName} has logged in.`
	}
	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out.`
	}
	addRequest(request){
		this.#requests.push(request);
		return this
	}
	// getter Methods
	getFirstName(){
		return `First Name: ${this.#firstName}`
	}
	getLastName(){
		return `Last Name: ${this.#lastName}`
	}
	getEmail(){
		return this.#email
	}
	getDepartment(){
		return `Department: ${this.#department}`
	}
	getStatus(){
		return `Status: ${this.#isActive}`
	}
	getRequests(){
		return this.#requests
	}
	// setter Methods
	setFirstName(firstName){
		this.#firstName = firstName
	}
	setLastName(lastName){
		this.#lastName = lastName
	}
	setEmail(email){
		this.#email = email;
	}
	setDepartment(department){
		this.#department = department;
	}
	setStatus(status){
		this.#isActive = status;
	}

}
let employee1 = new Employee("Ayin", "Medina", "ayin@mail.com", "CS");
let employee2 = new Employee("Paolo", "Valenzuela", "pao@mail.com", "IT");
employee1.addRequest(request1);
employee2.addRequest(request2);
console.log(employee1.getFullName());
console.log(employee1.login());
console.log(employee1.getRequests());
console.log(employee1.logout());



class TeamLead extends Person{
	#firstName;
	#lastName;
	#email;
	#department;
	#isActive;
	#members;
	constructor(firstName, lastName, email, department){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#isActive = true;
		this.#members = [];
/*		if(this.constructor === TeamLead){

		}*/
		if(this.addMember === undefined){
			throw new Error(
				"Class must implement addMember() method."
			);
		}
		if(this.checkRequests === undefined){
			throw new Error(
				"Class must implement checkRequests() method."
			);
		}
	}
	// getter Methods
	getFirstName(){
		return `First Name: ${this.#firstName}`
	}
	getLastName(){
		return `Last Name: ${this.#lastName}`
	}
	getEmail(){
		return this.#email
	}
	getDepartment(){
		return `Department: ${this.#department}`
	}
	getStatus(){
		return `Status: ${this.#isActive}`
	}
	getMembers(){
		return this.#members
	}
	// setter Methods
	setFirstName(firstName){
		this.#firstName = firstName
	}
	setLastName(lastName){
		this.#lastName = lastName
	}
	setEmail(email){
		this.#email = email;
	}
	setDepartment(department){
		this.#department = department;
	}
	setStatus(status){
		this.#isActive = status;
	}

	getFullName(){
		return `${this.#firstName} ${this.#lastName}`
	}
	login(){
		return `${this.#firstName} ${this.#lastName} has logged in.`
	}
	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out.`
	}
	addMember(employee){
		this.#members.push(employee);
		return this;
	}
	checkRequests(employeeEmail){
		/*let checkEmployee = this.#members.find(employee=>employee.#email===employeeEmail);
		if(checkEmployee===true){
			let emp = this.#members.find(employee=>employee.#email===employeeEmail);
			return emp.requests
		}else{
			return `${employeeEmail} does not exists.`
		}*/
		/*let emp = this.#members.findIndex(employee=>employee.getEmail()=== employeeEmail)*/
	    let empIndex = this.#members.findIndex(
			(member) => {
				return member.getEmail() === employeeEmail
			}
		);

	    if (empIndex !== -1) {
	    	let employee = this.#members[empIndex];
	    	return employee.getRequests();
	    } else {
	    	return `${employeeEmail} does not exists`;
	    }

     


	}
}
let teamLead1 = new TeamLead("Mango", "Medina", "mango@mail.com", "CS");
teamLead1.addMember(employee1);
teamLead1.addMember(employee2);
console.log(teamLead1.getFullName());
console.log(teamLead1.login());
console.log(teamLead1.getMembers());
console.log(teamLead1.checkRequests(employee1.getEmail()));
console.log(teamLead1.logout());


class Admin extends Person{
	#firstName;
	#lastName;
	#email;
	#department;
	#teamLeads;
	constructor(firstName, lastName, email, department){
		super();
		this.#firstName = firstName;
		this.#lastName = lastName;
		this.#email = email;
		this.#department = department;
		this.#teamLeads = [];
		if(this.addTeamLead === undefined){
			throw new Error(
				"Class must implement addTeamLead() method."
			);
		}
		if(this.deactivateTeam === undefined){
			throw new Error(
				"Class must implement deactivateTeam() method."
			);
		}
	}
	// getter Methods
	getFirstName(){
		return `First Name: ${this.#firstName}`
	}
	getLastName(){
		return `Last Name: ${this.#lastName}`
	}
	getEmail(){
		return this.#email
	}
	getDepartment(){
		return `Department: ${this.#department}`
	}
	getTeamLeads(){
		return this.#teamLeads
	}

	// setter Methods
	setFirstName(firstName){
		this.#firstName = firstName
	}
	setLastName(lastName){
		this.#lastName = lastName
	}
	setEmail(email){
		this.#email = email;
	}
	setDepartment(department){
		this.#department = department;
	}
	getFullName(){
		return `${this.#firstName} ${this.#lastName}`
	}
	login(){
		return `${this.#firstName} ${this.#lastName} has logged in.`
	}
	logout(){
		return `${this.#firstName} ${this.#lastName} has logged out.`
	}
	addTeamLead(teamLead){
		this.#teamLeads.push(teamLead);
		return this;
	}
	deactivateTeam(teamLeadEmail){
		let teamLeadIndex = this.#teamLeads.findIndex(
            (teamLead) => {
                return teamLead.getEmail() === teamLeadEmail
            }
        );

        if (teamLeadIndex !== -1) {
          let teamLead = this.#teamLeads[teamLeadIndex];
          teamLead.setStatus(false);
          teamLead.getMembers().forEach((member) => (member.setStatus(false)));
          this.#teamLeads.splice(teamLeadIndex, 1);
          return "Team deactivated successfully";
        } else {
          return `${teamLeadEmail} does not exists or is not a Team Leader.`;
        }
	}

}

let admin1 = new Admin("Hayley", "Williams", "hayley@mail.com", "Admin");
admin1.addTeamLead(teamLead1);
admin1.getTeamLeads();
console.log(admin1.getFullName());
console.log(admin1.login());
console.log(admin1.getTeamLeads());
console.log(admin1.deactivateTeam(teamLead1.getEmail()));
console.log(admin1.logout());

////////////////////////////////////////////////////////////

class RegularShape{
	constructor(){
		if(this.constructor === RegularShape){
			throw new Error(
				"Object cannot be created from an abstract class RegularShape"
			);
		}
		if(this.perimeter === undefined){
			throw new Error(
				"Class must implement perimeter() method."
			);
		}
		if(this.area === undefined){
			throw new Error(
				"Class must implement area() method."
			);
		}
	}
}


class Square extends RegularShape{
	constructor(noSides, length){
		super();
		this.noSides = noSides;
		this.length = length;
	}
	perimeter(){
		let perimeter = this.noSides * this.length;
		return `The perimeter of the square is ${perimeter}.`
	}
	area(){
		let area = this.length**2;
		return `The area of the square is ${area}.`
	}
}

let shape1 = new Square(4,16);
console.log(shape1.perimeter())
console.log(shape1.area())




////////////////////////////////////////////////////////////


class Food{
	constructor(name, price){
		this.name = name;
		this.price = price;
		if(this.getName === undefined){
			throw new Error(
				"Class must implement getName() method."
			);
		}
	}
}

class Vegetable extends Food{
	constructor(name, breed, price){
		super();
		this.name = name;
		this.breed = breed;
		this.price = price;
	}
	getName(){
		return `${this.name} is of ${this.breed} variety and is priced at ${this.price} pesos.`
	}
}

const vegetable1 = new Vegetable("Pechay", "Native", 25);
console.log(vegetable1.getName());



////////////////////////////////////////////////////////////


class Equipment{
	constructor(equipmentType, model){
		this.equipmentType = equipmentType;
		this.model = model;
		if(this.printInfo === undefined){
			throw new Error(
				"Class must implement printInfo() method."
			);
		}
	}
}

class Bulldozer extends Equipment{
	constructor(equipmentType, model, bladeType){
		super();
		this.equipmentType = equipmentType
		this.model = model;
		this.bladeType = bladeType;
	}
	printInfo(){
		return `Info: ${this.equipmentType}
The bulldozer ${this.model} has a ${this.bladeType} blade`
	}
}

let bulldozer1 = new Bulldozer("Bulldozer", "Brute", "Shovel");
console.log(bulldozer1.printInfo())

class TowerCrane extends Equipment{
	constructor(equipmentType, model, hookRadius, maxCapacity){
		super();
		this.equipmentType = equipmentType;
		this.model = model;
		this.hookRadius = hookRadius;
		this.maxCapacity = maxCapacity;
	}
	printInfo(){
		return `Info: ${this.equipmentType}
The tower crane ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`
	}
}

let towerCrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500)
console.log(towerCrane1.printInfo())
